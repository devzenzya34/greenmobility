# FULL SYMFONY PROJECT ( GREEN MOBILITY )

## Environnement de développement

### Pré-requis
* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

### Lancer l'environnement de développement
docker-compose up - d
symfony serve -d

### Lancer les tests
php bin/phpunit --testdox


### AJout de fixtures

composer require --dev orm-fixtures

symfony console doctrine:fixtures:load

### PRODUCTION
### Envoi de mail en CRON
symfony console app:send-contact
