<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AProposController extends AbstractController
{
    /**
     * @Route("/apropos", name="apropos")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('apropos/index.html.twig', [
            'agence' => $userRepository->getAgence(),
        ]);
    }
}
