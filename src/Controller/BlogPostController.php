<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\BlogPostRepository;
use App\Repository\CommentaireRepository;
use App\Services\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogPostController extends AbstractController
{
    /**
     * @Route("/blog", name="blog_post")
     */
    public function index(
        BlogPostRepository $blogPostRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $blogPostRepository->findBy([], ['createdAt' => 'DESC']);

        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('blog_post/index.html.twig', [
            'blogposts' => $blogposts,
        ]);
    }

    /**
     * @Route("/blog/{slug}", name="blog_detail")
     */
    public function details(
        BlogPost $blogPost,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {
        $commentaires = $commentaireRepository->findCommentaires($blogPost);

        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isSubmitted()) {
            $commentaire = $form->getData();
            //service dédié pour persister les comentaires
            $commentaireService->persistCommentaire($commentaire, $blogPost, null);

            return $this->redirectToRoute('blog_detail', ['slug' => $blogPost->getSlug()]);
        }

        return $this->render('blog_post/blogpost.html.twig', [
            'blogpost' => $blogPost,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
