<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use App\Repository\VehiculeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(VehiculeRepository $vehiculeRepository, BlogPostRepository $blogPostRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'vehicules' => $vehiculeRepository->lastThree(),
            'blogposts' => $blogPostRepository->lastThree()
        ]);
    }
}
