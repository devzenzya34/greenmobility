<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use App\Repository\CategorieRepository;
use App\Repository\VehiculeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(
        Request $request,
        VehiculeRepository $vehiculeRepository,
        BlogPostRepository $blogPostRepository,
        CategorieRepository $categorieRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost(); // récupère le hotname dans la requète http

        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('home')];//page acceuil
        $urls[] = ['loc' => $this->generateUrl('park')];
        $urls[] = ['loc' => $this->generateUrl('blog_post')];
        $urls[] = ['loc' => $this->generateUrl('apropos')];
        $urls[] = ['loc' => $this->generateUrl('contact')];

        //récupérer tout les slug de chaque véhicule pour générer leurs URLS
        foreach ($vehiculeRepository->findAll() as $vehicule) {
            $urls[] = [
                'loc' => $this->generateUrl('vehicule_details', ['slug' => $vehicule->getSlug()]),
                'lastmod' => $vehicule->getCreatedAt()->format('Y-m-d')
            ];
        }

        //récupérer tous les slig pour générer les URLS de blogpost
        foreach ($blogPostRepository->findAll() as $blogPost) {
            $urls[] = [
                'loc' => $this->generateUrl('blog_detail', ['slug' => $blogPost->getSlug()]),
                'lastmod' => $blogPost->getCreatedAt()->format('Y-m-d')
            ];
        }

        //récupérer tous les slig pour générer les URLS de catégorie
        foreach ($categorieRepository->findAll() as $categorie) {
            $urls[] = [
                'loc' => $this->generateUrl('park_categorie', ['slug' => $categorie->getSlug()])
            ];
        }

        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]), 200
        );

        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }
}
