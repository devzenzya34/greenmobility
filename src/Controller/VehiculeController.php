<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\Vehicule;
use App\Form\CommentaireType;
use App\Repository\CommentaireRepository;
use App\Repository\VehiculeRepository;
use App\Services\CommentaireService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VehiculeController extends AbstractController
{
    /**
     * @Route("/park", name="park")
     * récupère toutes les voitures
     */
    public function index(VehiculeRepository $vehiculeRepository): Response
    {
        return $this->render('vehicule/index.html.twig', [
            'vehicules' => $vehiculeRepository->findBy([], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/park/{slug}", name="park_categorie")
     */
    public function categorie(Categorie $categorie, VehiculeRepository $vehiculeRepository): Response
    {
        $vehicules = $vehiculeRepository->findAllPark($categorie);

        return $this->render('vehicule/categorie.html.twig', [
            'categorie' => $categorie,
            'vehicules' => $vehicules
        ]);
    }

    /**
     * @Route("/vehicule/{slug}", name="vehicule_details")
     * affiche le détail d'un véhicule)
     */
    public function details(
        Vehicule $vehicule,
        Request $request,
        CommentaireRepository $commentaireRepository,
        CommentaireService $commentaireService
    ): Response {
        $commentaires = $commentaireRepository->findCommentaires($vehicule);

        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isSubmitted()) {
            $commentaire = $form->getData();
            //service dédié pour persister les comentaires
            $commentaireService->persistCommentaire($commentaire, null, $vehicule);

            return $this->redirectToRoute('blog_detail', ['slug' => $vehicule->getSlug()]);
        }
        return $this->render('vehicule/vehicule.html.twig', [
            'vehicule' => $vehicule,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
