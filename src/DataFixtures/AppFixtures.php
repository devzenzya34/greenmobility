<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Categorie;
use App\Entity\User;
use App\Entity\Vehicule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Création d'un utilisateur(admin)
        $user = new User();

        $user->setEmail('admin@mail.com')
            ->setPrenom($faker->firstName)
            ->setNom($faker->lastName)
            ->setTelephone($faker->phoneNumber)
            ->setAPropos($faker->text())
            ->setInstagram('Mon Instagram')
            ->setRoles(['ROLE_AGENCE']);

        $password = $this->encoder->encodePassword($user, 'demorasa');
        $user->setPassword($password);

        $manager->persist($user);

        //Création de 10 articles
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new BlogPost();

            $blogpost->setTitre($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setContenu($faker->text(300))
                ->setSlug($faker->slug(3))
                ->setUser($user);

            $manager->persist($blogpost);
        }

        //création d'un blogpost pour les tests
        $blogpost = new BlogPost();

        $blogpost->setTitre('BlogPost Test')
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setContenu($faker->text(350))
            ->setSlug('blogpost-test')
            ->setUser($user);

        $manager->persist($blogpost);

        //boucle vehicule dans la boucle catégorie
        //création des catégories
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                ->setSlug($faker->words(10, true))
                ->setDescription($faker->slug());
            $manager->persist($categorie);

            //création de 2vehicules par catégories
            for ($j = 0; $j < 2; $j++) {
                $vehicule = new Vehicule();
                $vehicule->setNom($faker->words(3, true))
                    ->setAutonomie($faker->randomFloat(2, 20, 60))
                    ->setVitesseMax($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateDeSortie($faker->dateTimeBetween('-6 month', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPark($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('placeholder.png')
                    ->addCategorie($categorie)
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setUser($user);

                $manager->persist($vehicule);
            }
        }

        //Catégorie de test
        $categorie = new Categorie();

        $categorie->setNom('Categorie Test')
            ->setDescription($faker->words(10, true))
            ->setSlug('categorie-test');
        $manager->persist($categorie);

        //Véhicule pour les tests
        $vehicule = new Vehicule();

        $vehicule->setNom('Vehicule test')
            ->setAutonomie($faker->randomFloat(2, 20, 60))
            ->setVitesseMax($faker->randomFloat(2, 20, 60))
            ->setEnVente($faker->randomElement([true, false]))
            ->setDateDeSortie($faker->dateTimeBetween('-6 month', 'now'))
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text())
            ->setPark($faker->randomElement([true, false]))
            ->setSlug('vehicule-test')
            ->setFile('placeholder.png')
            ->addCategorie($categorie)
            ->setPrix($faker->randomFloat(2, 100, 9999))
            ->setUser($user);

        $manager->persist($vehicule);

        $manager->flush();
    }
}
