<?php

namespace App\EventSubscriber;

use App\Entity\BlogPost;
use App\Entity\Vehicule;
use DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setDateAndUser'],
        ];
    }

    public function setDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (($entity instanceof Blogpost)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);

            //On récupère le user courant
            $user = $this->security->getUser();
            $entity->setUser($user);
        }
        if (($entity instanceof Vehicule)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);

            //On récupère le user courant
            $user = $this->security->getUser();
            $entity->setUser($user);
        }
        return;
    }
}
