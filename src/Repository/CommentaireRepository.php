<?php

namespace App\Repository;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Vehicule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

    //récupère les commentaires soit dans les blogpost soit dans les véhicules
    public function findCommentaires($value)
    {
        if ($value instanceof BlogPost) {
            $object = 'blogpost';
        }
        if ($value instanceof Vehicule) {
            $object = 'vehicule';
        }

        return $this->createQueryBuilder('com')
            ->andWhere('com.' . $object . ' = :val')
            ->andWhere('com.isPublished = true')
            ->setParameter('val', $value->getId())
            ->orderBy('com.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
