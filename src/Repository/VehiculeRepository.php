<?php

namespace App\Repository;

use App\Entity\Categorie;
use App\Entity\Vehicule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vehicule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vehicule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vehicule[]    findAll()
 * @method Vehicule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehiculeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vehicule::class);
    }

    /**
     * @return Vehicule[] Returns an array of Vehicule Objects
     * Retourne les 3 derniers vehicules
     */
    public function lastThree()
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.id', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Categorie[] Returns an array of Vehicule Objects
     * récupe les véhicules d'une catégorie admis dans le park (park=true)
     */
    public function findAllPark(Categorie $categorie): array
    {
        return $this->createQueryBuilder('v')
            ->where(':categorie MEMBER OF v.categorie')
            ->andWhere('v.park = TRUE')
            ->setParameter('categorie', $categorie)
            ->getQuery()
            ->getResult();
    }
}
