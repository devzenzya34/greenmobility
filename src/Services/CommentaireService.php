<?php

namespace App\Services;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Vehicule;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        BlogPost $blogPost = null,
        Vehicule $vehicule = null
    ): void {
        $commentaire->setIsPublished(false)
            ->setBlogPost($blogPost)
            ->setVehicule($vehicule)
            ->setCreatedAt(new DateTime('now'));

        $this->manager->persist($commentaire);
        $this->manager->flush();
        $this->flash->add('success', 'Votre commentaire a bien été envoyé, il sera publié après modération');
    }

//    public function isSend(Contact $contact): void
//    {
//        $contact->setIsSend(true);
//
//        $this->manager->persist($contact);
//        $this->manager->flush();
//    }
}
