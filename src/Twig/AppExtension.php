<?php /** @noinspection MethodShouldBeFinalInspection */

namespace App\Twig;

use App\Repository\CategorieRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

//Extension twig permettant de stocker les vcatégories et de les transmettre plus facilement à toutes les vue
// (navbar présent sur toutes les pages)
class AppExtension extends AbstractExtension
{
    private $categorieRepository;

    public function __construct(CategorieRepository $categorieRepository)
    {
        $this->categorieRepository = $categorieRepository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('categorieNavbar', [$this, 'categorie'])
        ];
    }

    public function categorie(): array
    {
        return $this->categorieRepository->findAll();
    }
}
