<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AProposFunctionalTest extends WebTestCase
{
    public function testDisplayAPropos(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/apropos');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Who We Are');
    }
}