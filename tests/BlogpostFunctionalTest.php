<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BlogpostFunctionalTest extends WebTestCase
{
    public function testShouldDisplayBlogpost(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/blog');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('p', 'Toutes nos actualités');
    }

    public function testShouldDisplayBlogpostTest(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/blog/blogpost-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'BlogPost Test');
        $this->assertSelectorTextContains('p', 'blogpost-test');
    }
}
