<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Vehicule;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $categorie = new Categorie();

        $categorie->setNom('nom')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertTrue($categorie->getNom() === 'nom');
        $this->assertTrue($categorie->getDescription() === 'description');
        $this->assertTrue($categorie->getSlug() === 'slug');
    }

    public function testIsFalse(): void
    {
        $categorie = new Categorie();

        $categorie->setNom('nom')
            ->setDescription('description')
            ->setSlug('slug');

        $this->assertFalse($categorie->getNom() === 'false');
        $this->assertFalse($categorie->getDescription() === 'description false');
        $this->assertFalse($categorie->getSlug() === 'slug false');
    }

    public function testIsEmpty(): void
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getId());
    }

    public function testAddGetRemoveCategorie()
    {
        $vehicule = new Vehicule();
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getVehicules());

        $categorie->addVehicule($vehicule);
        $this->assertContains($vehicule, $categorie->getVehicules());

        $categorie->removeVehicule($vehicule);
        $this->assertEmpty($categorie->getVehicules());
    }
}
