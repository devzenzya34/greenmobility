<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Vehicule;
use Datetime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $vehicule = new Vehicule();
        $blogpost = new BlogPost();

        $commentaire->setAuteur('auteur')
            ->setContenu('contenu')
            ->setEmail('email@test.com')
            ->setCreatedAt($datetime)
            ->setBlogpost($blogpost)
            ->setVehicule($vehicule);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getVehicule() === $vehicule);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $vehicule = new Vehicule();
        $blogpost = new BlogPost();

        $commentaire->setAuteur('auteur')
            ->setContenu('contenu')
            ->setEmail('email@test.com')
            ->setCreatedAt($datetime)
            ->setBlogpost($blogpost)
            ->setVehicule($vehicule);

        $this->assertFalse($commentaire->getAuteur() === 'titrefalse');
        $this->assertFalse($commentaire->getContenu() === 'contenufalse');
        $this->assertFalse($commentaire->getEmail() === 'slugfalse');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getVehicule() === new Vehicule());
        $this->assertFalse($commentaire->getBlogpost() === new BlogPost());

    }

    public function testIsEmpty()
    {
        $blogpost = new Commentaire();
        $datetime = new DateTime();

        $this->assertEmpty($blogpost->getAuteur());
        $this->assertEmpty($blogpost->getContenu());
        $this->assertEmpty($blogpost->getEmail());
        $this->assertEmpty($blogpost->getCreatedAt());
        $this->assertEmpty($blogpost->getId());
    }
}
