<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\User;
use App\Entity\Vehicule;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setPrenom('prenom')
            ->setNom('nom')
            ->setPassword('pwd')
            ->setAPropos('test a propos')
            ->setInstagram('instagram')
            ->setTelephone('0909090909')
            ->setRoles(['ROLE_TEST']);

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'pwd');
        $this->assertTrue($user->getAPropos() === 'test a propos');
        $this->assertTrue($user->getInstagram() === 'instagram');
        $this->assertTrue($user->getTelephone() == '0909090909');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);

    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setPrenom('prenom')
            ->setNom('nom')
            ->setPassword('pwd')
            ->setAPropos('test a propos')
            ->setInstagram('instagram');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPassword() === 'pwd2');
        $this->assertFalse($user->getAPropos() === 'false a propos');
        $this->assertFalse($user->getInstagram() === 'facebook');

    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getUsername());
    }

    public function testAddGetRemoveVehicule()
    {
        $vehicule = new Vehicule();
        $user = new User();

        $this->assertEmpty($user->getVehicules());

        $user->addVehicule($vehicule);
        $this->assertContains($vehicule, $user->getVehicules());

        $user->removeVehicule($vehicule);
        $this->assertEmpty($user->getVehicules());
    }

    public function testAddGetRemoveBlogpost()
    {
        $blogpost = new BlogPost();
        $user = new User();

        $this->assertEmpty($user->getBlogPosts());

        $user->addBlogPost($blogpost);
        $this->assertContains($blogpost, $user->getBlogPosts());

        $user->removeBlogPost($blogpost);
        $this->assertEmpty($user->getBlogPosts());
    }
}
