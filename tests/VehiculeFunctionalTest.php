<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VehiculeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayPark(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/park');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Tous nos véhicules');
    }

    public function testShouldDisplayCategorieVehicule(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/park/categorie-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Notre flotte par catégories');
    }

    public function testShouldDisplayVehicule(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/vehicule/vehicule-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'vehicule-test');
    }
}
