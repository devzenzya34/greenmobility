<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\User;
use App\Entity\Vehicule;
use PHPUnit\Framework\TestCase;

class VehiculeUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $vehicule = new Vehicule();
        $datetime = new \DateTime();
        $categorie = new Categorie();
        $user = new User();

        $vehicule->setNom('nom')
            ->setAutonomie(20.20)
            ->setVitesseMax(22.22)
            ->setEnVente(true)
            ->setDateDeSortie($datetime)
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setPark(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setPrix(20.20)
            ->setUser($user);

        $this->assertTrue($vehicule->getNom() === 'nom');
        $this->assertTrue($vehicule->getAutonomie() == 20.20);
        $this->assertTrue($vehicule->getVitesseMax() == 22.22);
        $this->assertTrue($vehicule->getEnVente() === true);
        $this->assertTrue($vehicule->getDateDeSortie() === $datetime);
        $this->assertTrue($vehicule->getCreatedAt() === $datetime);
        $this->assertTrue($vehicule->getDescription() === 'description');
        $this->assertTrue($vehicule->getPark() === true);
        $this->assertTrue($vehicule->getSlug() === 'slug');
        $this->assertTrue($vehicule->getFile() === 'file');
        $this->assertTrue($vehicule->getPrix() == 20.20);
        $this->assertTrue($vehicule->getUser() === $user);
        $this->assertContains($categorie, $vehicule->getCategorie());
    }

    public function testIsEmpty()
    {
        $vehicule = new Vehicule();

        $this->assertEmpty($vehicule->getNom());
        $this->assertEmpty($vehicule->getAutonomie());
        $this->assertEmpty($vehicule->getVitesseMax());
        $this->assertEmpty($vehicule->getEnVente());
        $this->assertEmpty($vehicule->getDateDeSortie());
        $this->assertEmpty($vehicule->getCreatedAt());
        $this->assertEmpty($vehicule->getDescription());
        $this->assertEmpty($vehicule->getPark());
        $this->assertEmpty($vehicule->getSlug());
        $this->assertEmpty($vehicule->getFile());
        $this->assertEmpty($vehicule->getPrix());
        $this->assertEmpty($vehicule->getCategorie());
        $this->assertEmpty($vehicule->getUser());
        $this->assertEmpty($vehicule->getId());
    }

    public function testAddGetRemoveComentaire()
    {
        $vehicule = new Vehicule();
        $commentaire = new Commentaire();

        $this->assertEmpty($vehicule->getCommentaires());

        $vehicule->addCommentaire($commentaire);
        $this->assertContains($commentaire, $vehicule->getCommentaires());

        $vehicule->removeCommentaire($commentaire);
        $this->assertEmpty($vehicule->getCommentaires());
    }

    public function testAddGetRemoveCategorie()
    {
        $vehicule = new Vehicule();
        $categorie = new Categorie();

        $this->assertEmpty($vehicule->getCategorie());

        $vehicule->addCategorie($categorie);
        $this->assertContains($categorie, $vehicule->getCategorie());

        $vehicule->removeCategorie($categorie);
        $this->assertEmpty($vehicule->getCategorie());
    }
}

//faire de même que les autres entités pour assertfalse et assertempty

